# How ODK Works?
***

* Open a web browser and navigate to http://localhost:80. A login page will appear as follows:
![alt text](https://gitlab.com/arunvk007/icfoss_contributions/-/raw/main/Odk%20Form/images/img1.png)

* Type your login credentials and click Login

* After logging in, a home page will appears as follow:
![alt text](https://gitlab.com/arunvk007/icfoss_contributions/-/raw/main/Odk%20Form/images/img2.png)

* Click 'New' to create a new project

* Then you need to enter a name for your project
![alt text](https://gitlab.com/arunvk007/icfoss_contributions/-/raw/main/Odk%20Form/images/img3.png)

* After that click 'Create', Then you will be redirected to a new page as below:
![alt text](https://gitlab.com/arunvk007/icfoss_contributions/-/raw/main/Odk%20Form/images/img4.png)

* Click 'New' to create a new Form.
![alt text](https://gitlab.com/arunvk007/icfoss_contributions/-/raw/main/Odk%20Form/images/img5.png)

* I have attached a template file in xlsx format. Upload the file

* You can modify the file content as needed. Instructions for making modifications are provided within the file itself.

* After each modification you made, save the file and upload it again to apply the changes to the form.

* The form will look like this
![alt text](https://gitlab.com/icfoss1434642/odk-central/-/raw/main/images/img8.png)

* You can download the xml file of the same form and if you need to add more complex modifications to the form, you can edit the xml file accordigly and upload the same.

* To view the submitted data, Go to:
![alt text](https://gitlab.com/icfoss1434642/odk-central/-/raw/main/images/img9.png)
![alt text](https://gitlab.com/icfoss1434642/odk-central/-/raw/main/images/img10.png)